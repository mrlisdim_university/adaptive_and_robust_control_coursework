import sympy as sp
from sympy import Matrix, Symbol, pprint

sp.init_printing()

A = Matrix([[0, 1],
            [1, -2]])

b = Matrix([[0], 
            [1]])

C = Matrix([[2, 1]])

K_fb = Matrix([[24.04, 7.6]])
I = sp.eye(2)
s = Symbol('s')


W = C * (I * s - (A - b * K_fb)).inv() * b
W = W[0];
W = W.together()

n = sp.numer(W).expand()
d = sp.denom(W).expand()

n_coeffs = sp.Poly(n, s).coeffs()
d_coeffs = sp.Poly(d, s).coeffs()

print("W(s) = ")
pprint(n/d)
print("\nNum coeffs:")
print(list(map(lambda x: sp.N(x, 5), n_coeffs)))
print("Denom coeffs:")
print(list(map(lambda x: sp.N(x, 5), d_coeffs)))