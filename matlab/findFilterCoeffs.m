% This script is a part of the project
% Entry point located in main.m file

% Newton polinomial
% s^4 + 4 omega_0 s^3 + 6 omega_0^2 s^2 + 4 omega_0^3 s + omega_0^4
t_newton = 7.8; % s
omega_0 = t_newton / tFilter_required;

% k = [ 4 * omega_0
%       6 * omega_0^2
%       4 * omega_0^3
%       omega_0^4     ];
 
k = [ omega_0^4
      4 * omega_0^3
      6 * omega_0^2
      4 * omega_0   ];
 
disp('Filter coeffs:');
Utils.dispVar(k);