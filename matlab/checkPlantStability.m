% This script is a part of the project
% Entry point located in main.m file

plantPoles = pole(plantSsModel);

disp('Plant poles is:');
disp(plantPoles);

isStable = true;
for i=1:length(plantPoles)
    if real(plantPoles(i)) > 0
        isStable = false;
        break;
    end
end

if isStable
    disp('Real part < 0, model is stable.');
else
    disp('Real part > 0, model is unstable.');
end