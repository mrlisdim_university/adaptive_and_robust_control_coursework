clear;
IMG_PATH = '../doc/img/generated/';

%% Plant model
n = 2;

A = [0  1
     1 -2];

b = [0 
     1];

C = [2 1];

D = 0;

plantSsModel = ss(A, b, C, D);
plantModel = tf(plantSsModel);

%% Check plant model
checkPlantStability();                                  Utils.tr;
checkPlantControlability();                             Utils.tr;
checkPlantObservability();                              Utils.tr;


%% Reference model
t_required = 1; % s

findReferenceModel();                                   Utils.tr;

%% Feedback Coeffs
findFeedbackCoeffs();                                   Utils.tr;


%% Sim and print y and u for stable autonomus plant model
simTime = 2; %s
isStableModelSimulationEnable = false;
isPrintingEnable = false;
Sim.stableModelSimulation();

%% Signal generator

%%%   Generator full signal:
%
% g(t) = 3*sin(t) + cos(t)

%%%   First part of signal
%
% g1(t) = 3*sin(t)
%
% Sytem:
% { dw1 = w2
% { dw2 = -16 * w1
% { g1 = w1
%
% SS from:
% { dw = Gamma1 * w12
% { g1 = h1 * w12
% 
% w12 = [w1]
%       [w2]

Gamma1 = [ 0   1
          -16  0];
h1 = [1 0];

t_init = 0;
w12_init = [3 * sin(4 * t_init)
            12 * cos(4 * t_init)];

%%%   Second part of  signal
%
% g2(t) = cos(t)
%
% Sytem:
% { dz1 = w4
% { dz2 = -w3
% { g2 = w3
%
% SS from:
% { dz = Gamma2 * w34
% { g2 = h2 * w34
% 
% w34 = [w3]
%       [w4]

Gamma2 = [ 0   1
          -1   0];
h2 = [1 0];

w34_init = [ cos(t_init)
            -sin(t_init) ];
      
%%%   Generator full signal:
Gamma = [Gamma1                zeros(size(Gamma1)) 
         zeros(size(Gamma2))   Gamma2            ];

h = [1 0 1 0];

b_g = zeros(length(h), 1);

w_init = [w12_init
          w34_init];

 
%% g(t) sim and print
simTime = 10; %s
isSignalGeneratorModelSimulation = false;
isPrintingEnable = false;
Sim.signalGeneratorModelSimulation();


%% Matrix L_g and M_g
L_g = [ -0.10
         1.05
        -0.40
         1.20 ];

M_g = [0.1	-0.05	0.4   -0.2
       0.8   0.10   0.2    0.4];

Utils.dispVar(L_g);
Utils.dispVar(M_g);                                    Utils.tr;


%% Generator input-output and param forms
%
% r0 g(t) + r2 g^(2)(t) + g^(4)(t) = 0

r = [ 16
      0
      17
      0 ];

% g = theta^T xi
% theta = [k0 - r0]
%         [k1 - r1]
%         [k2 - r2]
%         [k3 - r3]
%
% xi = [ 1/K(s) ]
%      [ s/K(s) ]
%      [s^2/K(s)]
%      [s^3/K(s)]
%
% K(s) = s^4 + k3 s^3 + k2 s^2 + k1 s + k0;

tFilter_required = 3; %s
findFilterCoeffs();                                    Utils.tr;

theta = k - r;

% dxi = G * xi + l * g
  
G = [  0     1     0     0
       0     0     1     0
       0     0     0     1
      -k'                  ];

l = [0
     0
     0
     1];

%% Adaptation
sigma = 0.0000001;
gamma = 100000;

% W(s) = C * (I * s - (A - b * K_fb))^-1 * b =
%
%          1061.68 + 973.21 s + 267.264 s^2 + 23.04 s^3
% = ----------------------------------------------------------- =
%   12230.6 + 10192.2 s + 3185.05 s^2 + 442.368 s^3 + 23.04 s^4
%
% W(s)^-1 = 7.6 + 
%           s +
%           7.83959/(1.99998 + s) + 
%           (0.00476191 + 0.000412749 s)/(23.0402 + 9.60002 s + s^2)

% Decomposed Ws_inv (part1 + ... + part4 = Ws^-1)
Ws_inv_part1 = 7.6;   % just a gain
Ws_inv_part2 = [1 0]; % derivative
Ws_inv_part3_num = [7.83959];
Ws_inv_part3_den = [1 1.99998];
Ws_inv_part4_num = [0.000412749 0.00476191];
Ws_inv_part4_den = [1 9.60002 23.0402];

% Num and denum of Ws
Ws_num = [23.04 267.264 973.21 1061.68];
Ws_den = [23.04 442.368 3185.05 10192.2 12230.6];

%% y(t) and e(t) sim and print 
simTime = 200; %s
isAdaptiveModelSimulation = false;
isPrintingEnable = false;
Sim.adaptiveModelSimulation();
 
 
 
 
