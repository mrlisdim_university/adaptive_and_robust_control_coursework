% This script is a part of the project
% Entry point located in main.m file

plantModelControlableMatr = ctrb(plantSsModel);

disp('Controlable matrix is');
disp(plantModelControlableMatr);

if rank(plantModelControlableMatr) == n
    display('rank == 2, plant is controlable');
else
    display('rank != 2, plant is not controlable');
end
    