% This script is a part of the project
% Entry point located in main.m file

a0 = -A(2,1);
a0_ref = -A_ref(2,1);

a1 = -A(2,2);
a1_ref = -A_ref(2,2);

K_fb = [a0_ref - a0, a1_ref - a1];

disp('Feedback coeffs:');
dispVar(K_fb);

%K_acker = [0 1] * inv(plantModelControlableMatr) * ...
%          (A^2 + a1_ref * A + a0_ref * eye(2));