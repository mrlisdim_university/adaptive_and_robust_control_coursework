% This script is a part of the project
% Entry point located in main.m file

% Newton polinomial
% s^2 + 2 omega_0 s + omega_0 s^2
t_newton = 4.8; % s
omega_0 = t_newton / t_required;

% Reference system
A_ref = [ 0            1
         -omega_0^2   -2*omega_0];

b_ref = [0 
         1];

C_ref = [1 0];

fprintf('omega_0 = t_new / t_req = %.3f/%.3f = %.3f\n\n', ...
        t_newton, t_required, omega_0);

disp('Reference system:');
Utils.dispVar(A_ref);
Utils.dispVar(b_ref);
Utils.dispVar(C_ref);