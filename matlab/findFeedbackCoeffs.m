% This script is a part of the project
% Entry point located in main.m file

a0_ref = -A_ref(2,1);
a1_ref = -A_ref(2,2);

% Using Ackermann's formula
K_fb = [0 1] * inv(plantModelControlableMatr) * ...
       (A^2 + a1_ref * A + a0_ref * eye(2));

disp('Feedback coeffs:');
Utils.dispVar(K_fb);