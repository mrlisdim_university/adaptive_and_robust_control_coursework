% This script is a part of the project
% Entry point located in main.m file

if isSignalGeneratorModelSimulation
    sim('models/signalGeneratorModel');

    config = struct();
    config.xy = [g_dataset.time g_dataset.signals(1).values];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$y(t)$$";
    y_figure = buildGraph("", config, true);
    
    if isPrintingEnable      
        fullPath = sprintf('%s%s', IMG_PATH, 'g.eps');
        print(y_figure, fullPath, '-depsc');
    end
end