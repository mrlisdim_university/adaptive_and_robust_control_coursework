% This script is a part of the project
% Entry point located in main.m file

if isAdaptiveModelSimulation
    sim('models/adaptiveModel');

    dataset = e_g_y_dataset;
    
    config = struct();
    config.xy = [dataset.time dataset.signals(1).values];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$e(t)$$";
    e_figure = buildGraph("", config, true);
    
    config.xy = [dataset.time dataset.signals(3).values];
    config.xyLim = [0 10];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$y(t)$$";
    g_y_figure = buildGraph("", config, true);
    
    config.xy = [dataset.time dataset.signals(2).values];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$g(t)$$";
    buildGraph("", config, false);
    
    if isPrintingEnable
        fullPath = sprintf('%s%s', IMG_PATH, 'e.eps');
        print(e_figure, fullPath, '-depsc');
        
        fullPath = sprintf('%s%s', IMG_PATH, 'g_y.eps');
        print(g_y_figure, fullPath, '-depsc');
    end
end