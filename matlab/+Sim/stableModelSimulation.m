% This script is a part of the project
% Entry point located in main.m file

if isStableModelSimulationEnable
    sim('models/plantStableModel');

    config = struct();
    config.xy = [y_dataset.time y_dataset.signals.values];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$y(t)$$";
    y_figure = buildGraph("", config, true);
    
    
    config.xy = [u_dataset.time u_dataset.signals.values];
    config.xLabel = "$$t, s$$";
    config.yLabel = "$$u(t)$$";
    u_figure = buildGraph("", config, true);
    
    if isPrintingEnable      
        fullPath = sprintf('%s%s', IMG_PATH, 'y.eps');
        print(y_figure, fullPath, '-depsc');
        
        fullPath = sprintf('%s%s', IMG_PATH, 'u.eps');
        print(u_figure, fullPath, '-depsc');
    end
end