% This script is a part of the project
% Entry point located in main.m file

plantModelObservableMatr = obsv(plantSsModel);

disp('Observable matrix is');
disp(plantModelObservableMatr);

if rank(plantModelObservableMatr) == n
    display('rank == 2, plant is observable');
else
    display('rank != 2, plant is not observable');
end
    